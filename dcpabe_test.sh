
#!/bin/bash
# test the scripts

set -e

TMPDIR="$(mktemp -d)"
echo $TMPDIR
DIR_UNIVERSE=${TMPDIR}/universe
DIR_AUTH1=${TMPDIR}/auth1
DIR_AUTH2=${TMPDIR}/auth2
DIR_USER1=${TMPDIR}/user1
DIR_USER2=${TMPDIR}/user2

mkdir $DIR_UNIVERSE
mkdir $DIR_AUTH1
mkdir $DIR_AUTH2
mkdir $DIR_USER1
mkdir $DIR_USER2

echo "TESTING:"
echo " * universe setup"
python dcpabe_universe_setup.py >${DIR_UNIVERSE}/pubparams

echo " * authority 1 setup"
cp ${DIR_UNIVERSE}/pubparams ${DIR_AUTH1}
python dcpabe_authority_setup.py ${DIR_AUTH1}/pubparams STUDENT PROFESSOR >${DIR_AUTH1}/pub_key1 2>${DIR_AUTH1}/master1

echo " * authority 2 setup"
cp ${DIR_UNIVERSE}/pubparams ${DIR_AUTH2}
python dcpabe_authority_setup.py ${DIR_AUTH2}/pubparams CITIZEN >${DIR_AUTH2}/pub_key2 2>${DIR_AUTH2}/master2

echo " * authority 1 priv key generation for user 1"
python dcpabe_genprivkey.py ${DIR_AUTH1}/pubparams ${DIR_AUTH1}/master1 'user1' STUDENT >${DIR_AUTH1}/priv_key1
cp ${DIR_AUTH1}/priv_key1 ${DIR_USER1}

echo " * authority 2 priv key generation for user 1"
python dcpabe_genprivkey.py ${DIR_AUTH2}/pubparams ${DIR_AUTH2}/master2 'user1' CITIZEN >${DIR_AUTH2}/priv_key2
cp ${DIR_AUTH2}/priv_key2 ${DIR_USER1}

echo " * encryption"
cp ${DIR_UNIVERSE}/pubparams ${DIR_USER2}
cp ${DIR_AUTH1}/pub_key1 ${DIR_USER2}
cp ${DIR_AUTH2}/pub_key2 ${DIR_USER2}
python dcpabe_encrypt.py ${DIR_USER2}/pubparams 'user2' '(STUDENT or CITIZEN)' ${DIR_USER2}/pub_key1 ${DIR_USER2}/pub_key2  >${DIR_USER2}/ct 2>${DIR_USER2}/psk
cp ${DIR_USER2}/ct ${DIR_USER1}

echo " * decryption"
cp ${DIR_UNIVERSE}/pubparams ${DIR_USER1}
python dcpabe_decrypt.py ${DIR_USER1}/pubparams ${DIR_USER1}/ct ${DIR_USER1}/priv_key1 ${DIR_USER1}/priv_key2 >${DIR_USER1}/psk2
echo

cat ${DIR_USER2}/psk ${DIR_USER1}/psk2

if diff ${DIR_USER2}/psk ${DIR_USER1}/psk2; then
    echo "TEST SUCCEEDED"
else
    echo "TEST FAILED"
fi

rm -r "${TMPDIR}"

