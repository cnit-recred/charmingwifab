#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from dcpabeJSON import AuthorityJ
import sys

if len(sys.argv) < 3:
    print "Setup authority with the attributes {attr1, attr2, ..., attrN}"
    print "Usage:   %s <pub params file> <attr1> <attr2> ... <attrN>" % sys.argv[0]
    print "Example: %s pubparams student professor >pub_key1 2>master1" % sys.argv[0]
    sys.exit(1)

jpubparamsfile = sys.argv[1]
attributes = sys.argv[2:]

jpubparams = ''.join(open(jpubparamsfile).readlines())

a = AuthorityJ(jpubparams)
a.setup(attributes)

apub = a.get_public_key()
master = a.dump()

print apub
print >> sys.stderr, master

