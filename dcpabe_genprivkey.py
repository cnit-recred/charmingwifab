#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from dcpabeJSON import AuthorityJ
import sys

if len(sys.argv) < 5:
    print "Generate user private key with the attributes {attr1, attr2, ..., attrN}"
    print "Usage:   %s <pub params file> <authority private params file> <gid> <attr1> <attr2> ... <attrN>" % sys.argv[0]
    print "Example: %s pubparams master1 \"user1\" student professor >priv_key1" % sys.argv[0]
    sys.exit(1)

jpubparamsfile = sys.argv[1]
jprivparamsfile = sys.argv[2]
gid = sys.argv[3]
attributes = sys.argv[4:]

jpubparams = ''.join(open(jpubparamsfile).readlines())
jprivparams = ''.join(open(jprivparamsfile).readlines())

a = AuthorityJ(jpubparams, jprivparams)
print a.keygen(gid, attributes)

