#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from dcpabeJSON import UniverseJ
import sys

u = UniverseJ()
u.setup()

print u.get_params()

