#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from abencJSON import UserJ
import sys

if len(sys.argv) < 3:
    print "Generate a random string and encode it according to the access policy"
    print "Usage:   %s <pub params file> <access policy>" % sys.argv[0]
    print "Example: %s pub_key \"(STUDENT or PROFESSOR)\" >ct 2>psk" % sys.argv[0]
    sys.exit(1)

pubparamsfile = sys.argv[1]
accesspolicy = sys.argv[2]

pubparameters = ''.join(open(pubparamsfile).readlines())
pubparameters = pubparameters.strip()

u = UserJ()

u.setkey(pubparameters)

h, ct = u.generate_and_encrypt(accesspolicy)

print >> sys.stderr,  h
print ct


