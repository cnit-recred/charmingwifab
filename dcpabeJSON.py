#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from JSONserializer import jsonserialize, jsondeserialize, json_extract_curve
import dcpabe0
from dcpabe0 import *
import sys

class UniverseJ():
    def __init__(self):
        self.universe0 = dcpabe0.Universe()

    def setup(self, jpubparams=None):
        if jpubparams == None:
            self.universe0.setup()
        else:
            curve = json_extract_curve(jpubparams)
            pubparams = jsondeserialize(jpubparams)
            self.universe0.setup(curve=curve, g=pubparams['g'])
        self.groupobj = self.universe0.groupobj
    
    def get_gp(self):
        return jsonserialize({'gp': self.gp})

    def get_params(self):
        curve, g = self.universe0.get_params()
        return jsonserialize({'curve': curve, 'g': g}, self.groupobj)

    def dump(self):
        return self.get_params()


class AuthorityJ():
    def __init__(self, jpubparams, jprivparams=None):
        curve = json_extract_curve(jpubparams)
        pubparams = jsondeserialize(jpubparams)
        g = pubparams['g']
        pk = None
        mk = None
        if jprivparams != None:
            privparams = jsondeserialize(jprivparams)
            pk = privparams['pk']
            mk = privparams['mk']
        self.authority0 = dcpabe0.Authority(curve, g, pk, mk)
        self.groupobj = self.authority0.groupobj

    def setup(self, attrs):
        self.authority0.setup(attrs)

    def get_public_key(self):
        pk, curve, g = self.authority0.get_public_key()
        return jsonserialize({'pk': pk, 'curve': curve, 'g': g}, self.groupobj)

    def keygen(self, gid, attrs):
        K = self.authority0.keygen(gid, attrs)
        return jsonserialize({'K': K}, self.groupobj)

    def dump(self):
        pk, curve, g = self.authority0.get_public_key()
        return jsonserialize({
                              'pk': pk,
                              'curve': curve,
                              'g': self.authority0.gp['g'],
                              'mk': self.authority0.mk
                             }, self.groupobj)

class UserJ():
    def __init__(self, jpubparams, jprivparams=None, gid=""):
        curve = json_extract_curve(jpubparams)
        groupobj = PairingGroup(curve)
        pubparams = jsondeserialize(jpubparams, groupobj)
        K = None
        if jprivparams != None:
            privparams = jsondeserialize(jprivparams, groupobj)
            K = privparams['K']
            gid = K['gid']
        g = pubparams['g']
        self.user0 = dcpabe0.User(gid, curve, g)
        self.groupobj = self.user0.groupobj
        if K != None:
            self.user0.add_key(K)

    def get_gid(self):
        return self.user0.gid

    def add_key(self, jK):
        Kd = jsondeserialize(jK, self.groupobj)
        K = Kd['K']
        self.user0.add_key(K)

    def decrypt(self, ctj):
        ctd = jsondeserialize(ctj, self.groupobj)
        # print "DES", ctd
        return self.user0.decrypt(ctd['ct'])

    def generate_and_encrypt(self, access_policy):
        h, ct = self.user0.generate_and_encrypt(access_policy)
        # print "PRE", ct
        js = jsonserialize({'ct': ct}, self.groupobj)
        return h, js

    def add_pub_key(self, pkj):
        pks = jsondeserialize(pkj, self.groupobj)
        pk = pks['pk']
        self.user0.add_pub_key(pk)

    def dump(self):
        d = {}
        d['gid'] = self.user0.gid
        d['curve'] = self.user0.curve
        d['K'] = self.user0.K
        d['g'] = self.user0.gp['g']
        d['pk'] = self.user0.pk
        dj = jsonserialize(d, self.groupobj)
        return dj



if __name__ == "__main__":
    # create the universe
    a0j = UniverseJ()
    a0j.setup()
    jpubparams = a0j.get_params()

    del a0j
    
    # create authority 1 for its attributes
    a1j = AuthorityJ(jpubparams)
    auth1_attrs = ['ONE', 'TWO']
    a1j.setup(auth1_attrs)
    a1jpub = a1j.get_public_key()
    a1master = a1j.dump()

    del a1j
    
    # create authority 1 for its attributes
    a2j = AuthorityJ(jpubparams)
    auth2_attrs = ['THREE', 'FOUR']
    a2j.setup(auth2_attrs)
    a2jpub = a2j.get_public_key()
    a2master = a2j.dump()
    
    del a2j

    # generate private key for user1 from authority 1
    usr_attrs1 = ['ONE', 'TWO']
    a1j = AuthorityJ(jpubparams, a1master)
    k1j = a1j.keygen('user1', usr_attrs1)

    del a1j

    # generate private key for user1 from authority 2
    usr_attrs2 = ['THREE']
    a2j = AuthorityJ(jpubparams, a2master)
    k2j = a2j.keygen('user1', usr_attrs2)

    del a2j

    # encrypt
    u2j = UserJ(jpubparams, gid='user2')
    # add public keys from authorities
    u2j.add_pub_key(a1jpub)
    u2j.add_pub_key(a2jpub)
    # generate and encrypt
    policy = '((ONE or THREE) and (TWO or FOUR))'
    h, ctj = u2j.generate_and_encrypt(policy)
    
    print "CT", ctj

    del u2j

    # decrypt
    u2j = UserJ(jpubparams, gid='user1')
    # add private keys from authorities
    u2j.add_key(k1j)
    u2j.add_key(k2j)
    # decryption
    rec_h = u2j.decrypt(ctj)

    print h, "\n", rec_h

    assert h == rec_h

