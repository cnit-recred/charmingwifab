#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from abencJSON import AuthorityJ
import sys

if len(sys.argv) < 3:
    print "Create private key with the attributes {attr1, attr2, ..., attrN}"
    print "Usage:   %s <master params file> <attr1> <attr2> ... <attrN>" % sys.argv[0]
    print "Example: %s master student >stud_key" % sys.argv[0]
    sys.exit(1)

masterparamsfile = sys.argv[1]
attributes = sys.argv[2:]

masterparams = ''.join(open(masterparamsfile).readlines())

a = AuthorityJ()
a.setup(masterparams)

print a.keygen(attributes)


