#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from abencJSON import UserJ
import sys

if len(sys.argv) < 4:
    print "Decrypt an ABE ciphertext"
    print "Usage:   %s <pub params file> <private key file> <ciphertext file>" % sys.argv[0]
    print "Example: %s pub_key stud_key ct > pskd" % sys.argv[0]
    sys.exit(1)

pubparamsfile = sys.argv[1]
privparamsfile = sys.argv[2]
ctfile = sys.argv[3]

pubparameters = ''.join(open(pubparamsfile).readlines())
pubparameters = pubparameters.strip()

privparameters = ''.join(open(privparamsfile).readlines())
privparameters = privparameters.strip()

ctj = ''.join(open(ctfile).readlines())
ctj = ctj.strip()

u = UserJ()

u.setkey(pubparameters, privparameters)
rec_h = u.decrypt(ctj)

print rec_h

