#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from charm.core.engine.util import *
from charm.toolbox.pairinggroup import PairingGroup
import json


def jsonserialize(thisobject, go):
    return json.dumps(serializeObject(thisobject, go))

def json_extract_curve(element):
    # extract the curve from the json
    j = json.loads(element)
    c = j['curve']
    cu = str(c.split(':')[1]).strip()
    return cu

def jsondeserialize(jsonstring, go=None):
    if go == None:
        curve = json_extract_curve(jsonstring)
        go = PairingGroup(curve)
    return deserializeObject(json.loads(jsonstring), go)

