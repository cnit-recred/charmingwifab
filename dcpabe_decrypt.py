#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from dcpabeJSON import UserJ
import sys
import json

if len(sys.argv) < 4:
    print "Decrypt"
    print "Usage:   %s <pub params file> <ciphertext file > <user priv key 1> <user priv key 2> ... <user priv key N>" % sys.argv[0]
    print "Example: %s pubparams ct priv_key1 priv_key2 >pskd" % sys.argv[0]
    sys.exit(1)

jpubparamsfile = sys.argv[1]
ctfile = sys.argv[2]
privkeyfiles = sys.argv[3:]

jpubparams = ''.join(open(jpubparamsfile).readlines())
ctj = ''.join(open(ctfile).readlines())

privkeys = [''.join(open(privkeyfile).readlines()) for privkeyfile in privkeyfiles]

u = UserJ(jpubparams, privkeys[0])

for privkey in privkeys:
    u.add_key(privkey)

rec_h = u.decrypt(ctj)

print rec_h



