#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from dcpabeJSON import UserJ
import sys

if len(sys.argv) < 5:
    print "Generate and encrypt a nonce"
    print "Usage:   %s <pub params file> <gid> <policy> <pub key 1> <pub key 2> ... <pub key N>" % sys.argv[0]
    print "Example: %s pubparams \"user2\" \"(student or professor\") pub_key1 pub_key2 >ct 2>psk" % sys.argv[0]
    sys.exit(1)

jpubparamsfile = sys.argv[1]
gid = sys.argv[2] # XXX: do we need this to encrypt?
policy = sys.argv[3]
pubkeyfiles = sys.argv[4:]

jpubparams = ''.join(open(jpubparamsfile).readlines())
pubkeys = [''.join(open(pubkeyfile).readlines()) for pubkeyfile in pubkeyfiles]

u = UserJ(jpubparams, gid=gid)

for pk in pubkeys:
    u.add_pub_key(pk)

h, ctj = u.generate_and_encrypt(policy)

print >> sys.stderr, h
print ctj

