#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

import charm
from charm.schemes.abenc.abenc_bsw07 import *
import sys
import hashlib

CURVE = 'SS512'

class Authority():
    def setup(self, curve=None, pk=None, mk=None):
        if curve and pk and mk:
            self.curve = curve
            self.groupobj = PairingGroup(self.curve)
            self.cpabe = CPabe_BSW07(self.groupobj)
            self.pk = pk
            self.mk = mk
        else:
            self.curve = CURVE
            self.groupobj = PairingGroup(self.curve)
            self.cpabe = CPabe_BSW07(self.groupobj)
            (self.pk, self.mk) = self.cpabe.setup()

    def get_pub_key(self):
        return self.pk, CURVE

    def keygen(self, attrs):
        sk = self.cpabe.keygen(self.pk, self.mk, attrs)
        return sk


class User():
    def setkey(self, pk, curve, sk=None):
        self.pk = pk
        self.groupobj = PairingGroup(curve)
        self.sk = sk
        self.cpabe = CPabe_BSW07(self.groupobj)

    def decrypt(self, ct):
        # print "CTI ", ct.__class__, ct
        # print "PKI", self.pk
        # print "SKI", self.sk
        rec_msg = self.cpabe.decrypt(self.pk, self.sk, ct)
        # print "REC_MSG ", rec_msg
        rec_d = hashlib.sha256(self.groupobj.serialize(rec_msg)).digest()
        rec_h = ''.join(["%02x" % ord(c) for c in rec_d])
        return rec_h

    def generate_and_encrypt(self, access_policy):
        rand_msg = self.groupobj.random(GT)
        d = hashlib.sha256(self.groupobj.serialize(rand_msg)).digest()
        h = ''.join(["%02x" % ord(c) for c in d])
        ct = self.cpabe.encrypt(self.pk, rand_msg, access_policy)
        #print "CTG", ct.__class__, ct
        return h, ct


if __name__ == "__main__":

    a = Authority()
    u1 = User()  # encrypts
    u2 = User()  # decrypts

    attruniverse = ['ONE', 'TWO', 'THREE', 'FOUR']  # unused
    a.setup()

    pk, curve = a.get_pub_key()

    attrs = ['ONE', 'TWO', 'THREE']
    sk = a.keygen(attrs)
    u2.setkey(pk, curve, sk)

    u1.setkey(pk, curve)
    access_policy = '((four or three) and (three or one))'
    h, ct = u1.generate_and_encrypt(access_policy)

    rec_h = u2.decrypt(ct)

    print h, "\n", rec_h


