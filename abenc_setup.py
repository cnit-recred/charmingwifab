#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from abencJSON import AuthorityJ
import sys

if len(sys.argv) > 1:
    print "Create public and private parameters for the attribute universe {attr1, attr2, ..., attrN}"
    print "Usage:   %s" % sys.argv[0]
    print "Example: %s >pub_key 2>master" % sys.argv[0]
    sys.exit(1)

a = AuthorityJ()

a.setup()

print a.get_pub_key()
print >> sys.stderr, a.dump()

