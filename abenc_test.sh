#!/bin/bash
# test the scripts

set -e

TMPDIR="$(mktemp -d)"

echo $TMPDIR
DIR_AUTH=${TMPDIR}/auth
DIR_USER1=${TMPDIR}/user1
DIR_USER2=${TMPDIR}/user2

mkdir $DIR_AUTH
mkdir $DIR_USER1
mkdir $DIR_USER2

echo "TESTING:"
echo " * setup"
python abenc_setup.py >${DIR_AUTH}/pub_key 2>${DIR_AUTH}/master

echo " * priv key generation"
python abenc_genprivkey.py ${DIR_AUTH}/master STUDENT > ${DIR_AUTH}/stud_key

echo " * encryption"
cp ${DIR_AUTH}/pub_key ${DIR_USER2}
python abenc_encrypt.py ${DIR_USER2}/pub_key '(STUDENT or PROFESSOR)' >${DIR_USER2}/ct 2>${DIR_USER2}/psk
cp ${DIR_USER2}/ct ${DIR_USER1}

echo " * decryption"
cp ${DIR_AUTH}/pub_key ${DIR_USER1}
cp ${DIR_AUTH}/stud_key ${DIR_USER1}
python abenc_decrypt.py ${DIR_USER1}/pub_key ${DIR_USER1}/stud_key ${DIR_USER1}/ct >${DIR_USER1}/psk2

echo

cat ${DIR_USER2}/psk ${DIR_USER1}/psk2

if diff ${DIR_USER2}/psk ${DIR_USER1}/psk2; then
    echo "TEST SUCCEEDED"
else
    echo "TEST FAILED"
fi

rm -r "${TMPDIR}"

