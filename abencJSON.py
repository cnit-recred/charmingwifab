#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

import abenc0
import sys
import base64
from abenc0 import *
from JSONserializer import jsonserialize, jsondeserialize, json_extract_curve

class AuthorityJ():
    def __init__(self):
        self.authority0 = abenc0.Authority()

    def setup(self, jpubparams=None):
        if jpubparams == None:
            self.authority0.setup()
        else:
            curve = json_extract_curve(jpubparams)
            pubparams = jsondeserialize(jpubparams)
            self.authority0.setup(curve=curve, pk=pubparams['pk'], mk=pubparams['mk'])
        self.groupobj = self.authority0.groupobj

    def get_pub_key(self):
        pk, curve = self.authority0.get_pub_key()
        return jsonserialize({'pk': pk, 'curve': curve}, self.groupobj)

    def keygen(self, attrs):
        sk = self.authority0.keygen(attrs)
        # print "SKA", sk
        return jsonserialize({'sk': sk}, self.groupobj)

    def dump(self):
        pk, curve = self.authority0.get_pub_key()
        return jsonserialize({'pk': pk, 'curve': curve, 'mk': self.authority0.mk}, self.groupobj)



class UserJ():
    def __init__(self):
        self.user0 = abenc0.User()

    def setkey(self, jpubparams, jprivparams=None):
        curve = json_extract_curve(jpubparams)
        groupobj = PairingGroup(curve)
        self.groupobj = groupobj

        pubparams = jsondeserialize(jpubparams, groupobj)

        if jprivparams != None:
            privparams = jsondeserialize(jprivparams, groupobj)
            sk = privparams['sk']
        else:
            sk = None

        # print pubparams['pk']
        # print pubparams['curve']
        self.user0.setkey(pubparams['pk'], pubparams['curve'], sk)

    def decrypt(self, ctj):
        ctd = jsondeserialize(ctj, self.groupobj)
        # print "DES", ctd
        return self.user0.decrypt(ctd['ct'])

    def generate_and_encrypt(self, access_policy):
        h, ct = self.user0.generate_and_encrypt(access_policy)
        # print "PRE", ct
        js = jsonserialize({'ct': ct}, self.groupobj)
        return h, js


if __name__ == "__main__":
    # setup
    aj = AuthorityJ()

    aj.setup()

    jpubparams = aj.get_pub_key()
    jmaster = aj.dump()

    del aj

    # priv key generation
    aj1 = AuthorityJ()
    aj1.setup(jmaster)

    attrs = ['STUDENT']
    # user priv key
    jprivkey = aj1.keygen(attrs)

    del aj1

    # generate nonce and encrypt
    uj2 = UserJ()  
    uj2.setkey(jpubparams)
    access_policy = '(STUDENT OR PROFESSOR)'
    h, ctj = uj2.generate_and_encrypt(access_policy)

    print "CT", ctj

    del uj2

    # decrypt
    uj1 = UserJ()  # decrypts
    uj1.setkey(jpubparams, jprivkey)
    rec_h = uj1.decrypt(ctj)

    print h, "\n", rec_h

    assert h == rec_h

