#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

from charm.toolbox.pairinggroup import PairingGroup
from abenc0 import Authority, User
import json

a = Authority()

attruniverse = ['ONE', 'TWO', 'THREE', 'FOUR']
a.setup(attruniverse)

pk, curve = a.getPubKey()

go = PairingGroup(curve)

#print pk['h']

pks = go.serialize(pk['h'])
print type(pks), pks

j = json.dumps({'h': pks})

del pks

pks = json.loads(j)
pkd = go.deserialize(str(pks))
print type(pkd), pkd

