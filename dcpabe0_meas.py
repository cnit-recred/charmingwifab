#!/usr/bin/env python
#
# Copyright (C) 2017 Claudio Pisa <claudio.pisa@uniroma2.it>, Alberto Caponi <alberto.caponi@uniroma2.it>
#
# This is free software, licensed under the GNU General Public License v3.

import charm
from charm.schemes.abenc.dabe_aw11 import Dabe
from charm.toolbox.pairinggroup import PairingGroup, ZR, G1, G2, GT, pair, extract_key
import sys
import hashlib
import time

CURVE = 'SS512'

charm.schemes.abenc.dabe_aw11.debug = False
debug = False

class Universe():
    def setup(self, curve=None, g=None):
        if curve and g:
            self.gp = {}
            self.curve = curve
            self.groupobj = PairingGroup(self.curve)
            self.dabe = Dabe(self.groupobj)
            self.gp['g'] = g
            self.gp['H'] = lambda x: self.groupobj.hash(x, G1)
        else:
            self.curve = CURVE
            self.groupobj = PairingGroup(self.curve)
            self.dabe = Dabe(self.groupobj)
            self.gp = self.dabe.setup()
    
    def get_gp(self):
        return self.gp

    def get_params(self):
        return self.curve, self.gp['g']


class Authority():
    def __init__(self, curve, g, pk=None, mk=None):
        self.curve = curve
        self.groupobj = PairingGroup(self.curve)
        self.dabe = Dabe(self.groupobj)
        self.gp = {}
        self.gp['g'] = g
        self.gp['H'] = lambda x: self.groupobj.hash(x, G1)
        self.pk = pk
        self.mk = mk

    def setup(self, attrs):
        self.mk, self.pk = self.dabe.authsetup(self.gp, attrs)

    def get_public_key(self):
        return self.pk, self.curve, self.gp['g']

    def keygen(self, gid, attrs):
        K = {}
        for i in attrs:
            self.dabe.keygen(self.gp, self.mk, i, gid, K)
        return K


class User():
    def __init__(self, gid, curve, g):
        self.gid = gid
        self.curve = curve
        self.groupobj = PairingGroup(curve)
        self.K = {}
        self.dabe = Dabe(self.groupobj)
        self.gp = {}
        self.gp['g'] = g
        self.gp['H'] = lambda x: self.groupobj.hash(x, G1)
        self.pk = {}

    def add_key(self, K=None):
        self.K.update(K)

    def decrypt(self, ct):
        self.K.update({'gid': self.gid})

        orig_m = self.dabe.decrypt(self.gp, self.K, ct)

        h = extract_key(orig_m)
        
        d = hashlib.sha256(h).digest()
        dh = ''.join(["%02x" % ord(c) for c in d])

        return dh

    def generate_and_encrypt(self, access_policy):
        m = self.groupobj.random(GT)
        h = extract_key(m)
        d = hashlib.sha256(h).digest()
        dh = ''.join(["%02x" % ord(c) for c in d])

        ct = self.dabe.encrypt(self.gp, self.pk, m, access_policy)

        return dh, ct

    def add_pub_key(self, pk):
        self.pk.update(pk)


if __name__ == "__main__":
    f = open("dcpabecharm.csv", "a")
    # create the universe
    a0 = Universe()
    usetupstarttime = time.time()
    a0.setup()
    usetupendtime = time.time()
    curve, g = a0.get_params() # public parameters

    # create authority 1 for its attributes
    a1 = Authority(curve, g)
    auth1_attrs = ['ONE', 'TWO']
    setupstarttime = time.time()
    a1.setup(auth1_attrs)
    setupendtime = time.time()
    pk1, _, _ = a1.get_public_key()

    # create authority 2 for its attributes
    a2 = Authority(curve, g)
    auth2_attrs = ['THREE', 'FOUR']
    a2.setup(auth2_attrs)
    pk2, _, _ = a2.get_public_key()

    # create the users
    u1 = User("alice", curve, g)  # encrypts
    u2 = User("bob",   curve, g)  # decrypts

    # give attributes from a1 to u2
    usr_attrs1 = ['ONE', 'TWO']
    keygenstarttime = time.time()
    k1 = a1.keygen(u2.gid, usr_attrs1)
    keygenendtime = time.time()
    u2.add_key(k1)
    
    # give attributes from a2 to u2
    usr_attrs2 = ['THREE']
    k2 = a2.keygen(u2.gid, usr_attrs2)
    u2.add_key(k2)

    # u1 encrypts
    u1.add_pub_key(pk1)
    u1.add_pub_key(pk2)
    policy = '((ONE or THREE) and (TWO or FOUR))'
    encstarttime = time.time()
    h, ct = u1.generate_and_encrypt(policy)
    encendtime = time.time()

    # u2 decrypts
    decstarttime = time.time()
    rec_h = u2.decrypt(ct)
    decendtime = time.time()
    
    f.write("%.2f %2.f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n" % (usetupstarttime, usetupendtime, setupstarttime, setupendtime, keygenstarttime, keygenendtime, encstarttime, encendtime, decstarttime, decendtime))

    print "Encrypted:", h
    print "Decrypted:", rec_h

